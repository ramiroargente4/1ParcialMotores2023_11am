# Escalada

## Description

A 3D Platformer game where your objective is to climb to the summit of an obstacle course, dodging obstacles ranging from simple platforms to suicidal bomber enemies.

## Installation

Use Unity 2021.3.6f to load the files. Remember that it will take some time as the "library" folder is git-ignored
