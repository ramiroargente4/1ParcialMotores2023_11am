using UnityEngine;
using UnityEngine.AI;

public class ControlAgente : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent agente;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray rayo = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(rayo, out hit))
            {
                agente.SetDestination(hit.point);
            }
        }
    }
}
