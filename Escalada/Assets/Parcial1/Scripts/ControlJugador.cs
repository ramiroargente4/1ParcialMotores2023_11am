using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class ControlJugador : MonoBehaviour
{

    //JUGADOR
    private Rigidbody rb;
    public int rapidez;
    public TextMeshProUGUI textoCantidadRecolectados;
    public TextMeshProUGUI textoGanaste;

    public GameObject Jugador;
    public Transform JugadorTransform;

    //VICTORIA
    public int contVinilos;
    public int vinilosVictoria = 6; //Anteriormente la cantidad de vinilos necesaria para ganar. 
    // Actualmente la cantidad de vinilos totales. No hay una condicion que no ganes si recolectas menos. Influye en tu puntaje final

    //SALTO
    public LayerMask capaPiso;
    public float magnitudSalto;
    public SphereCollider col;
    public int chequeoPrimerSalto;
    public bool flagSalto;

    //IMPORTAR
    public ControlCamara controlCamara;
    public AnimarTexto animarTexto;
    public CheckpointsReinicios checkpointsReinicios;
    public ControlEnemigosAI controlEnemigosAI;
    public ProgramacionMenu programacionMenu;
    public TimerJuego timerJuego;

    //AVISOS
    private bool flagCamaraAvisoHecho = false;
    private bool flagSaltoAvisoHecho = false;
    private bool flagCaminoAvisoHecho = false;

    public bool flagTamañoRecolectado = false;
    public bool flagVelocidadRecolectada = false;
    public GameObject placaInformativa;

    //CUBOS SECRETOS
    public GameObject CubosSecretos;

    void Start()
    {
        //PRIMER INICIO

        //DESBLOQUEO
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


        //JUGADOR
        rb = GetComponent<Rigidbody>();
        contVinilos = 0;
        textoGanaste.text = "";

        //NARRACION
        ActualizacionVinilo();
        animarTexto.MostrarTexto("Bienvenido a Escalada!");

        //SALTO
        col = GetComponent<SphereCollider>();

        //CHECKPOINTS
        checkpointsReinicios.InicializarCheckpoints();

        //PLACA DE INFO
        placaInformativa.SetActive(false);
    }
    private void Update()
    {
        //SALTO
        Salto();
        DobleSalto();

        //CAMBIOS DE CAMARA
        CambiosCamara();

        //CAMBIOS DE TAMAÑO Y VELOCIDAD
        CambiosTamañoVelocidad();
    }

    //JUGADOR
    private void FixedUpdate()
    {
        CambiosImpulso();
    }

    private void CambiosCamara()
    {
        //INPUTS PARA CAMBIAR DE CAMARA

        //TERCERA PERSONA DESDE ATRAS
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            controlCamara.chequeoCambio = 0;

            controlCamara.xOffset = 0;
            controlCamara.zOffset = -1.35f;
            controlCamara.yOffset = 1f;
            Debug.Log("DESDE ATRAS");
        }
        //DESDE LA DERECHA
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            controlCamara.chequeoCambio = 1;

            controlCamara.xOffset = 1.65f;
            controlCamara.zOffset = 0;
            controlCamara.yOffset = 1;
            Debug.Log("DESDE LA DERECHA");
        }
        //DESDE ADELANTE
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            controlCamara.chequeoCambio = 2;

            controlCamara.xOffset = 0;
            controlCamara.zOffset = 1.65f;
            controlCamara.yOffset = 1;
            Debug.Log("DESDE ADELANTE");
        }
        //DESDE LA IZQUIERDA
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            controlCamara.chequeoCambio = 3;

            controlCamara.xOffset = -1.65f;
            controlCamara.zOffset = 0;
            controlCamara.yOffset = 1;
            Debug.Log("DESDE LA IZQUIERDA");
        }
        //AVISOS DE INFORMACION SOBRE CAMBIAR DE CAMARA
        if (Input.GetKeyDown(KeyCode.W) && animarTexto.flagAnimando == false && flagCamaraAvisoHecho == false)
        {
            animarTexto.MostrarTexto("Usa las flechas para cambiar tu camara");
            flagCamaraAvisoHecho = true;
        }

    }
    private void CambiosTamañoVelocidad()
    {
        if (flagTamañoRecolectado == true && Input.GetKeyDown(KeyCode.Alpha1))
        {
            JugadorTransform.localScale = new Vector3(0.593f, 0.593f, 0.593f);
        }
        if (flagTamañoRecolectado == true && Input.GetKeyDown(KeyCode.Alpha2))
        {
            JugadorTransform.localScale = new Vector3(0.383f, 0.383f, 0.383f);
        }
        if (flagVelocidadRecolectada == true && Input.GetKeyDown(KeyCode.Alpha3))
        {
            rapidez = 2;
        }
        if (flagVelocidadRecolectada == true && Input.GetKeyDown(KeyCode.Alpha4))
        {
            rapidez = 4;
        }
    }

    private void CambiosImpulso()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        if (controlCamara.chequeoCambio == 0)
        {
            //PRUEBA Y ERROR
            Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
            rb.AddForce(vectorMovimiento * rapidez);
        }
        if (controlCamara.chequeoCambio == 1)
        {
            Vector3 vectorMovimiento = new Vector3((movimientoVertical - (movimientoVertical * 2)), 0.0f, movimientoHorizontal);
            rb.AddForce(vectorMovimiento * rapidez);
        }
        if (controlCamara.chequeoCambio == 2)
        {
            Vector3 vectorMovimiento = new Vector3(movimientoHorizontal - (movimientoHorizontal * 2), 0.0f, movimientoVertical - (movimientoVertical * 2));
            rb.AddForce(vectorMovimiento * rapidez);
        }
        if (controlCamara.chequeoCambio == 3)
        {
            Vector3 vectorMovimiento = new Vector3(movimientoVertical, 0.0f, movimientoHorizontal - (movimientoHorizontal * 2));
            rb.AddForce(vectorMovimiento * rapidez);
        }
    }

    private void Salto()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
        //AVISOS DE INFORMACION SOBRE SALTO
        if (Input.GetKeyDown(KeyCode.Space) && animarTexto.flagAnimando == false && flagSaltoAvisoHecho == false)
        {
            animarTexto.MostrarTexto("Usa el espacio para saltar una o dos veces");
            flagSaltoAvisoHecho = true;
        }

    }
    private void DobleSalto()
    {
        if (EstaEnPiso() == false && flagSalto == false) //Solo entra al codigo en el salto despues de estar en el suelo
        {
            chequeoPrimerSalto = 1;
            flagSalto = true;
        }
        if (EstaEnPiso() == true) //Se reinician los chequeos una vez que se vuelva al suelo
        {
            chequeoPrimerSalto = 0;
            flagSalto = false;
        }
        if (chequeoPrimerSalto == 1 && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            chequeoPrimerSalto = 0; //Sobreescribo el valor, para que no entre devuelta a este seccion (si entra devuelta implicaria mas que un doble salto)
        }
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    //COLISIONES
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            contVinilos = contVinilos + 1;
            other.gameObject.SetActive(false);
            ActualizacionVinilo();
        }
        if (other.gameObject.CompareTag("paredroja") == true)
        {
            Jugador.SetActive(false);
            animarTexto.MostrarTexto("Has muerto! Presiona R para reintentarlo");
        }

        if (other.gameObject.CompareTag("cubosecreto") == true)
        {
            CubosSecretos.SetActive(true);
            if (flagCaminoAvisoHecho == false)
            {
                animarTexto.MostrarTexto("Ha aparecido un nuevo camino!");
                flagCaminoAvisoHecho = true;
            }
        }
        if (other.gameObject.CompareTag("cambiotamaño") == true)
        {
            other.gameObject.SetActive(false);
            flagTamañoRecolectado = true;
            JugadorTransform.localScale = new Vector3(0.383f,0.383f,0.383f); // Cambia el tamaño del Jugador
            animarTexto.MostrarTexto("Ahora puedes cambiar tu tamano con 1 y 2");
            placaInformativa.SetActive(true);
        }
        if (other.gameObject.CompareTag("cambiovelocidad") == true)
        {
            other.gameObject.SetActive(false);
            flagVelocidadRecolectada = true;
            rapidez = 4; //Cambia la velocidad del jugador
            animarTexto.MostrarTexto("Ahora puedes cambiar tu velocidad con 3 y 4");
            placaInformativa.SetActive(true);
        }
        checkpointsReinicios.chequeoUltimoCheckpoint(other);
        controlEnemigosAI.chequeoEnemigosCurva(other);
        controlEnemigosAI.chequeoEnemigosRampa(other);
        programacionMenu.ChequeoVictoria(other);
    }
    private void ActualizacionVinilo()
    {
        //UI
        textoCantidadRecolectados.text = "Vinilos recolectados: " + contVinilos.ToString();
        if (contVinilos < 5)
        {
            animarTexto.MostrarTexto("Recolectaste un vinilo, te faltan " + (vinilosVictoria - contVinilos).ToString());
        }
        if (contVinilos == 5)
        {
            animarTexto.MostrarTexto("Recolectaste todos los vinilos visibles.");
        }
        if (contVinilos == 6)
        {
            animarTexto.MostrarTexto("Recolectaste todos los vinilos del juego");
        }

    }
}
