using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigosAI : MonoBehaviour
{
    //IMPORTACIONES
    public CheckpointsReinicios checkpointsReinicios;
    public AnimarTexto animarTexto;

    //ENEMIGOS
    public GameObject botEnemigo;
    public GameObject botEnemigoMortal;

    public List<GameObject> listaEnemigos;
    public bool flagAparicionEnemigos;
    public bool flagEnemigosCurva;

    private bool flagEnemigoMortalAvisoHecho;

    //APARICION DE ENEMIGOS
    //private int contadorRampa = 0;
    public void chequeoEnemigosCurva(Collider other)
    {
        if (other.gameObject.CompareTag("enemigoscurva") && checkpointsReinicios.ultimoCheckPoint == 4 && flagEnemigosCurva == false)
        {
            AparecerEnemigosCurva();
            flagEnemigosCurva = true;
        }
    }
    public void chequeoEnemigosRampa(Collider other)
    {
        //UNA VEZ MAS, SERIA MEJOR HACERLO CON UNA LISTA DE TAGS O CON ALGUN CONTADOR PERO NO LO LOGRE:(
        if (other.gameObject.CompareTag("aparecedorrampa"))
        {
            AparecerEnemigosMortalesRampa(2.5f,3.2f);
        }
        if (other.gameObject.CompareTag("aparecedorrampa1"))
        {
            AparecerEnemigosMortalesRampa(2.7f, 2.9f);
            AparecerEnemigosMortalesRampa(2f, 3.4f);
        }
    }

    public void AparecerEnemigos()
    {
        if (checkpointsReinicios.ultimoCheckPoint == 4)
        {
            listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(2, 15f, 8f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(0, 15f, 8f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-2, 15f, 8f), Quaternion.identity));
            //listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-4, 15f, 8f), Quaternion.identity));
            animarTexto.MostrarTexto("Estos enemigos quieren tirarte!!!");
        }
    }

    public void AparecerEnemigosUnaVez() 
    { 
        if (checkpointsReinicios.ultimoCheckPoint == 4 && flagAparicionEnemigos == false)
        {
            flagAparicionEnemigos = true;
            listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(2, 15f, 8f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(0, 15f, 8f), Quaternion.identity));
            //listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-2, 15f, 8f), Quaternion.identity));
            //listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-4, 15f, 8f), Quaternion.identity));
            animarTexto.MostrarTexto("Estos enemigos quieren tirarte!!!");
        }
    }
    public void AparecerEnemigosCurva()
    {
        listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-5, 15f, 8f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-5, 15f, 6f), Quaternion.identity));
        //listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-5, 15f, 4f), Quaternion.identity));
        //listaEnemigos.Add(Instantiate(botEnemigo, new Vector3(-5, 15f, 2f), Quaternion.identity));
        animarTexto.MostrarTexto("Han aparecido mas!!");
    }
    public void AparecerEnemigosMortalesRampa(float coordenadaX, float coordenadaZ)
    {
        listaEnemigos.Add(Instantiate(botEnemigoMortal, new Vector3(coordenadaX,16.2f,coordenadaZ), Quaternion.identity));

        if (flagEnemigoMortalAvisoHecho == false)
        {
            animarTexto.MostrarTexto("Estos parecen mas peligrosos...");
            flagEnemigoMortalAvisoHecho = true;
        }
    }
}
