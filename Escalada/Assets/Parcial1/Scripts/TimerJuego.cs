using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerJuego : MonoBehaviour
{
    public float tiempo = 0f;
    //public float valorCronometro;
    public TextMeshProUGUI textoTimer;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ComenzarCronometro());
    }

    // Update is called once per frame
    void Update()
    {
        textoTimer.text = "Tiempo transcurrido: " + tiempo.ToString();
    }
    public IEnumerator ComenzarCronometro()
    {
        //tiempo = valorCronometro;
        while (tiempo >= 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo++;
        }
    }
}
