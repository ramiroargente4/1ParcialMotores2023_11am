using UnityEngine;

public class ControlBot : MonoBehaviour
{
    //private int hp;
    private GameObject jugador;
    public GameObject bot;
    public int rapidez;

    void Start()
    {
        //hp = 100;
        jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("paredroja"))
       {
            Destroy(bot);
       }
    }

}
