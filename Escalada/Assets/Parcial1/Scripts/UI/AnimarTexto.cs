using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class AnimarTexto : MonoBehaviour
{
    //TEXTOS EN LA UI Y FUNCIONAMIENTO
    public TextMeshProUGUI textMeshProAnimado;
    public float velocidadLetra = 0.1f;
    public float tiempoDeVisualizacion = 3.0f; // Cuanto tiempo el usuario ve el texto
    private string textoOriginal;
    private Coroutine animacionCoroutine;

    public bool flagAnimando;

    // Start is called before the first frame update
    void Start()
    {
        textoOriginal = textMeshProAnimado.text;
    }

    IEnumerator AnimarTextoUI()
    {
        foreach (char letra in textoOriginal)
        {
            textMeshProAnimado.text += letra;
            yield return new WaitForSeconds(velocidadLetra);
        }

        yield return new WaitForSeconds(tiempoDeVisualizacion);

        // Borrar el texto letra por letra
        for (int i = textMeshProAnimado.text.Length - 1; i >= 0; i--)
        {
            textMeshProAnimado.text = textMeshProAnimado.text.Remove(i);
            yield return new WaitForSeconds(velocidadLetra);
        }

        animacionCoroutine = null;
        flagAnimando = false;

        if (flagAnimando == false)
        {
            textMeshProAnimado.text = "Recolecta vinilos y llega a la cima!";
        }
    }
    public void MostrarTexto(string mensaje)
    {
        // Detener la corrutina anterior si hay una en curso
        if (animacionCoroutine != null)
        {
            StopCoroutine(animacionCoroutine);
        }

        textMeshProAnimado.text = "";
        textoOriginal = mensaje;
        animacionCoroutine = StartCoroutine(AnimarTextoUI());
        flagAnimando = true;
    }
}
