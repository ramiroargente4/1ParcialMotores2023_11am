using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ProgramacionMenu : MonoBehaviour
{
    //IMPORTACIONES SCRIPTS
    public ControlJugador controlJugador;
    public CheckpointsReinicios checkpointsReinicios;
    public TimerJuego timerJuego;
    public ControlCamara controlCamara;

    public GameObject Menu; //Responde al canvas completo del menu (incluye MenuUI y StatsUI)
    public GameObject Juego; //Responde a la totalidad del juego

    public GameObject MenuUI;
    public GameObject StatsUI;

    //FONDOS
    public Animator fondoImagen;
    public Animator fondoNegro;
    public Animator fondoBlancoVioleta;
    public Animator fondoBlanco;
    public Animator fondoTransABlanco;

    public GameObject fondoImagenNegra;
    public GameObject fondoNegroABlanco;
    public GameObject fondoTransBlanco;

    //TEXTOS PARA CALCULOS DE STATS
    public TextMeshProUGUI statsGanaste;
    public TextMeshProUGUI statsVinilos;
    public TextMeshProUGUI statsReinicios;
    public TextMeshProUGUI statsTiempo;
    public TextMeshProUGUI statsPuntaje;
    public TextMeshProUGUI statsPuntajeMasAlto;

    public float tiempoFinal;
    public float puntajeFinal;
    public float puntajeMasAlto = 0; // Se inicializa como 0


    // Start is called before the first frame update
    void Start()
    {
        fondoBlancoVioleta.SetBool("animFondoVioleta", true);
        Menu.SetActive(true);
        MenuUI.SetActive(true); //Entra en el menu y no en stats
        StatsUI.SetActive(false);

        Juego.SetActive(false);
        Cursor.lockState = CursorLockMode.None; //Bloqueo mouse

        //Inicializo todas los textos en 0
        statsGanaste.text = "No hay estadisticas";
        statsVinilos.text = "";
        statsReinicios.text = "";
        statsTiempo.text = "";
        statsPuntaje.text = "";
        statsPuntajeMasAlto.text = "";

    }

    //BOTONES
    public void IrAlJuego()
    {
        fondoImagenNegra.SetActive(true);
        fondoImagen.SetBool("animFondoMenu", true);
        fondoNegro.SetBool("animFondoNegro", true);
        StartCoroutine(CargarEscenaDespuesDeRetraso(4f));
    }
    public void IrAlMenuUI()
    {
        MenuUI.SetActive(true);
        StatsUI.SetActive(false);
    }
    public void IrAStatsUI()
    {
        MenuUI.SetActive(false);
        StatsUI.SetActive(true);
    }
    public void SalirUI()
    {
        Application.Quit();
    }

    //COMPORTAMIENTOS DE CARGA
    IEnumerator CargarMenuDespuesDeRetraso(float retrasoVictoria)
    {
        yield return new WaitForSeconds(retrasoVictoria);

        //Setteos generales
        fondoTransBlanco.SetActive(false); // No puedo encontrar la manera de hacer que se desactive la animacion cuando vuelvas AL JUEGO sin tener que apagar el gameobject
        Juego.SetActive(false); // Desactivo todas las caracteristicas del juego
        Cursor.lockState = CursorLockMode.None; //Desbloqueo mouse

        //Activo el canvas del menu
        Menu.SetActive(true);
        StatsUI.SetActive(true); //Activo la pestaña de stats
        MenuUI.SetActive(false); //Desactivo la pestaña del menu
        MostrarEstadisticas();

        //Configurar animators
        fondoImagenNegra.SetActive(false);
        fondoBlancoVioleta.SetBool("animFondoVioleta", false);
        fondoImagen.SetBool("animFondoMenu", false);
        fondoBlanco.SetBool("animFondoBlanco", true);
        fondoNegroABlanco.SetActive(false); //Un animator que deja de funcionar correctamente
    }

    IEnumerator CargarEscenaDespuesDeRetraso(float retraso)
    {
        yield return new WaitForSeconds(retraso);
        Menu.SetActive(false);
        Juego.SetActive(true);
        controlJugador.contVinilos = 0;
        fondoBlanco.SetBool("animFondoBlanco", true);

        //INICIO
        timerJuego.tiempo = 0;
        timerJuego.ComenzarCronometro(); //Reiniciamos el tiempo

        controlJugador.JugadorTransform.position = new Vector3(0f, 2f, 3f); //Ajustamos la posicion (la ultima del usuario una vez reiniciado va a ser en la parte alta del mapa)
        checkpointsReinicios.JugadorRigidBody.velocity = Vector3.zero; //Sin inercia
        //controlCamara.chequeoCambio = 0; //Camara para adelante

        controlJugador.flagTamañoRecolectado = false; // Tiene que volver a recolectar el powerup
        checkpointsReinicios.ZonaFinal.SetActive(false); 
        checkpointsReinicios.ultimoCheckPoint = 0;
        //checkpointsReinicios.InicializarCheckpoints(); // Hay que ver si es necesario re-inicializarlos. En teoria con reiniciar las flags alcanza

        //Reiniciamos las flags
        checkpointsReinicios.listaDeFlags[0] = true;
        checkpointsReinicios.listaDeFlags[1] = false;
        checkpointsReinicios.listaDeFlags[2] = false;
        checkpointsReinicios.listaDeFlags[3] = false;
        checkpointsReinicios.listaDeFlags[4] = false;

    }
    public void ChequeoVictoria(Collider other)
    {
        if (other.gameObject.CompareTag("suelovictoria"))
        {
            fondoTransABlanco.SetBool("animTransABlanco", true); //Animacion de transparente a blanco que te acerca a dios
            tiempoFinal = timerJuego.tiempo; //Guardo el tiempo final 
            StartCoroutine(CargarMenuDespuesDeRetraso(3f));
        }
    }
    public void MostrarEstadisticas()
    {
        puntajeFinal = (controlJugador.contVinilos * 200) - (checkpointsReinicios.contadorReinicios * 10) - (tiempoFinal * 2);

        if (puntajeFinal >= puntajeMasAlto) 
        {
            puntajeMasAlto = puntajeFinal;
        }

        statsGanaste.text = "Has ganado!";
        statsVinilos.text = "Vinilos recolectados: " + controlJugador.contVinilos + "/" + controlJugador.vinilosVictoria;
        statsReinicios.text = "Veces reiniciado el juego: " + checkpointsReinicios.contadorReinicios;
        statsTiempo.text = "Tiempo transcurrido: " + tiempoFinal;
        statsPuntaje.text = "Tu puntaje final es de " + puntajeFinal;
        statsPuntajeMasAlto.text = "Tu maximo puntaje conseguido es " + puntajeMasAlto;
    }

}
