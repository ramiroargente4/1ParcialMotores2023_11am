using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PisoDesaparecedor : MonoBehaviour
{

    //SUELO DESAPARECE
    public float tiempoEncima;
    //public GameObject SueloDesaparecer;
    public TextMeshProUGUI textoTiempo;
    public bool flagSobreSuelo;
    public bool flagFueLlamado;

    // Start is called before the first frame update
    void Start()
    {
        textoTiempo.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (flagSobreSuelo == true)
        {
            textoTiempo.text = "En " + tiempoEncima.ToString() + " se cae!";
        }
        if (tiempoEncima == 0)
        {
            flagSobreSuelo = false;
            gameObject.SetActive(false);
            StopCoroutine(TimerSuelo());

            //Reinicio a los valores iniciales
            tiempoEncima = 5;
            textoTiempo.text = "";
            flagFueLlamado = false;
        }

    }
    public IEnumerator TimerSuelo()
    {
        while (tiempoEncima <= 5 && tiempoEncima >= 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoEncima--;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador") == true && flagFueLlamado == false) //Flag fue llamado para que no entre eternamente y rompa el timer
        {
            StartCoroutine(TimerSuelo());
            flagSobreSuelo = true;
            flagFueLlamado = true;
        }
    }

}
