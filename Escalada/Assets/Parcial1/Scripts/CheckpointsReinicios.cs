using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointsReinicios : MonoBehaviour
{
    //JUGADOR
    public GameObject Jugador;
    public Transform JugadorTransform;
    public Rigidbody JugadorRigidBody;

    //IMPORTS
    public AnimarTexto animarTexto;
    public ControlEnemigosAI controlEnemigosAI;
    public List<PisoDesaparecedor> pisosDesaparecedores;

    //REINICIOS Y CHECKPOINTS
    public List<Transform> listaDeCheckPoints = new List<Transform>();
    private List<Vector3> posicionCheckpoints = new List<Vector3>();
    public List<bool> listaDeFlags = new List<bool>();
    public List<Animator> listaDeBanderas = new List<Animator>();
    public int ultimoCheckPoint;
    public int contadorReinicios;

    //ZONAS
    public GameObject ZonaFinal;


    // Start is called before the first frame update
    void Start()
    {
        ZonaFinal.SetActive(false); //Despues se va a activar

        ultimoCheckPoint = 0;
    }
    public void Update()
    {
        //CHEQUEOS DE REINICIO Y CHECKPOINTS
        ChequeosReinicios();
    }

    //CHEQUEOS DE REINICIO Y CHECKPOINTS
    public void ChequeosReinicios()
    {
        Vector3 posicionJugador = JugadorTransform.position;
        int cantidadPisosDesaparecedores = pisosDesaparecedores.Count;
        //Si el usuario presiona R, se considera el ultimocheckpoint por el que paso y se lo traslada ahi.
        if (Input.GetKeyDown(KeyCode.R))
        {
            Vector3 posicionCheckpoint = posicionCheckpoints[ultimoCheckPoint]; //A partir de cual es el ultimo checkpoint, define una variable Vector 3
            JugadorTransform.position = new Vector3(posicionCheckpoint.x, posicionCheckpoint.y + 1.0f, posicionCheckpoint.z); //Esto entonces se usa para mover el usuario hasta alli, PERO pudiendole sumar uno en Y, asi no traspasa la plataforma
            Jugador.SetActive(true);
            JugadorRigidBody.velocity = Vector3.zero; //Detiene la inercia que conservaba el usuario

            if (ultimoCheckPoint == 4) //El checkpoint 4 es el ultimo
            {
                foreach (GameObject item in controlEnemigosAI.listaEnemigos) // REMUEVE LOS QUE YA ESTAN Y APARECE NUEVOS
                {
                    Destroy(item);
                }
                controlEnemigosAI.flagEnemigosCurva = false; //Reinicia el chequeo de los enemigos curva
                controlEnemigosAI.AparecerEnemigos();
            }
            animarTexto.MostrarTexto("Mas reinicios = Menos puntos!");
            contadorReinicios++;

            //Tomando de la lista de objetos que tienen el script piso desaparecedor
            //Si llenaba una sola variable, solo podia reiniciar una de las plataformas.
            foreach (PisoDesaparecedor pisoDesaparecedor in pisosDesaparecedores)
            {
                pisoDesaparecedor.gameObject.SetActive(true); //Reiniciar los pisos desaparecedores si el usuario reinicia
            }
        }
    }
    public void InicializarCheckpoints()
    {
        foreach (Transform checkPoint in listaDeCheckPoints) //Por cada objeto CheckPoint a�adido a la lista en el Inspector, un Transform de CheckPoint
        {
            posicionCheckpoints.Add(checkPoint.position);
        }

        int cantidadDeCheckpoints = listaDeCheckPoints.Count; //Cantidad de checkpoints es lo mismo que la longitud total de la lista de checkpoints
        for (int i = 0; i < cantidadDeCheckpoints; i++) //Considerando todos los checkpoints que hay, una flag para cada uno.
        {
            listaDeFlags.Add(false); // La flag a�adida se inicializa en false
        }
    }
    public void chequeoUltimoCheckpoint(Collider other)
    {
        //ESTO SE PODRIA HACER MAS EFICIENTE ARMANDO UNA LISTA DE TAGS AUTO-GENERADOS QUE SE ASIGNEN A LOS OBJETOS
        // (NO SE COMO HACERLO)
        if (other.gameObject.CompareTag("0checkpoint") == true && listaDeFlags[0] == false)
        {
            ultimoCheckPoint = 0; //El ultimo checkpoint por el que paso es el primero 
            listaDeFlags[ultimoCheckPoint] = true; //Ya paso por este checkpoint
        }
        if (other.gameObject.CompareTag("1checkpoint") == true && listaDeFlags[1] == false)
        {
            ultimoCheckPoint = 1;
            listaDeFlags[ultimoCheckPoint] = true;
            listaDeBanderas[0].SetBool("animBandera1", true);
            animarTexto.MostrarTexto("Has alcanzado el checkpoint N " + ultimoCheckPoint.ToString());
        }
        if (other.gameObject.CompareTag("2checkpoint") == true && listaDeFlags[2] == false)
        {
            ultimoCheckPoint = 2;
            listaDeFlags[ultimoCheckPoint] = true;
            listaDeBanderas[0].SetBool("animBandera", false);
            listaDeBanderas[1].SetBool("animBandera2", true);
            //Comento esto para que no pise el aviso sobre la posibilidad de cambiar de tama�o
            //animarTexto.MostrarTexto("Has alcanzado el checkpoint N " + ultimoCheckPoint.ToString()); 
        }
        if (other.gameObject.CompareTag("3checkpoint") == true && listaDeFlags[3] == false)
        {
            ultimoCheckPoint = 3;
            listaDeFlags[ultimoCheckPoint] = true;
            listaDeBanderas[ultimoCheckPoint - 2].SetBool("animBandera2", false);
            listaDeBanderas[ultimoCheckPoint - 1].SetBool("animBandera3", true);
            animarTexto.MostrarTexto("Has alcanzado el checkpoint N " + ultimoCheckPoint.ToString());
        }
        if (other.gameObject.CompareTag("4checkpoint") == true && listaDeFlags[4] == false)
        {
            ZonaFinal.SetActive(true);

            ultimoCheckPoint = 4;
            listaDeFlags[ultimoCheckPoint] = true;
            listaDeBanderas[ultimoCheckPoint - 2].SetBool("animBandera3", false);
            listaDeBanderas[ultimoCheckPoint - 1].SetBool("animBandera4", true);
            animarTexto.MostrarTexto("Has alcanzado el checkpoint N " + ultimoCheckPoint.ToString());

            controlEnemigosAI.AparecerEnemigosUnaVez();
        }
    }
}
