using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    // Start is called before the first frame update
    public int factorMultiplicacionRotacion;
    public int identificadorTipoRotacion = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Gira en todos los ejes
        if (identificadorTipoRotacion == 0)
        {
            transform.Rotate(new Vector3(15, 60, 45) * (Time.deltaTime * factorMultiplicacionRotacion));
        }
        //Necesitaba uno que gire solo en X (Masomenos pero bueno que se le va a hacer)
        if (identificadorTipoRotacion == 1)
        {
            transform.Rotate(new Vector3(15, 0, 0) * (Time.deltaTime * factorMultiplicacionRotacion));
        }

    }
}
