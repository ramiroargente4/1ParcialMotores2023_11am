using UnityEngine;

public class ControlCubo : MonoBehaviour
{
    //MOVIMIENTO DESEADO
    public int movimientoDeseado; // Si configuras en 0, Arriba y Abajo, si configuras en 1, Derecha e Izquierda. Si configuras en 2, Rotacion.

    //ARRIBA Y ABAJO
    bool tengoQueBajar = false;
    public int rapidezArriba = 0;
    public int rapidezAbajo = 0;
    public float topeArriba;
    public float minimoAbajo;

    //DERECHA A IZQUIERDA
    bool tengoQueGirar = false;
    public int rapidezIzquierda = 0;
    public int rapidezDerecha = 0;
    public int topeDerecha = 0;
    public int minimoIzquierda = 0;

    //ROTACION
    public float rapidezRotacion = 0;

    //PROGRAMADO DE MANERA TAL QUE SE PUEDA CONFIGURAR EN EL INSPECTOR.

    void Update()
    {
        if (movimientoDeseado == 0) 
        {
            if (transform.position.y >= topeArriba)
            {
                tengoQueBajar = true;
            }
            if (transform.position.y <= minimoAbajo)
            {
                tengoQueBajar = false;
            }

            if (tengoQueBajar)
            {
                Bajar();
            }
            else
            {
                Subir();
            }
        }
        if (movimientoDeseado == 1) { 
            if (transform.position.x >= topeDerecha)
            {
                tengoQueGirar = true;
            }
            if (transform.position.x <= minimoIzquierda)
            {
                tengoQueGirar = false;
            }
            if (tengoQueGirar)
            {
                Izquierda();
            }
            else
            {
                Derecha();
            }
        }
        if (movimientoDeseado == 2)
        {
            RotacionLoca();
        }

    }
    void Subir()
    {
        transform.position += transform.up * rapidezArriba * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidezAbajo * Time.deltaTime;
    }
    void Derecha()
    {
        transform.position += transform.right * rapidezDerecha * Time.deltaTime;
    }
    void Izquierda()
    {
        transform.position -= transform.right * rapidezIzquierda * Time.deltaTime;
    }
    void RotacionLoca()
    {
        //VECTOR3.UP lo que hace es ignorar la coordenada X y Z para que solo sea afectado Y
        transform.Rotate(Vector3.up * rapidezRotacion * Time.deltaTime);
    }
}
