using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public GameObject Jugador;
    public float xOffset = 5.0f; 
    public float zOffset = 5.0f; 
    public float yOffset = 5.0f;

    public int chequeoCambio = 0;

    private Vector3 offset;

    void Start()
    {
        offset = transform.position - Jugador.transform.position;
    }

    void Update()
    {
        cambiosRotacionCamara();   
    }
    public void cambiosRotacionCamara()
    {
        Vector3 newPosition = Jugador.transform.position + new Vector3(xOffset, yOffset, zOffset);
        transform.position = newPosition;

        if (chequeoCambio == 0)
        {
            Vector3 rotacionInicial = new Vector3(25f, 0f, 0f);
            Quaternion newRotacionInicial = Quaternion.Euler(rotacionInicial);
            transform.rotation = newRotacionInicial;

        }
        if (chequeoCambio == 1)
        {
            Vector3 rotacionDerecha = new Vector3(20f, -90f, 0f);
            Quaternion newRotacionDerecha = Quaternion.Euler(rotacionDerecha);
            transform.rotation = newRotacionDerecha;
        }
        if (chequeoCambio == 2)
        {
            Vector3 rotacionTotal = new Vector3(20f, -180f, 0f);
            Quaternion newRotationTotal = Quaternion.Euler(rotacionTotal);
            transform.rotation = newRotationTotal;
        }
        if (chequeoCambio == 3)
        {
            Vector3 rotacionDerecha = new Vector3(20f, 90f, 0f);
            Quaternion newRotacionDerecha = Quaternion.Euler(rotacionDerecha);
            transform.rotation = newRotacionDerecha;
        }
    }
}
